FROM node:carbon
RUN apt-get install git-core 
WORKDIR /usr/src/app
RUN git clone https://iepctsergio@bitbucket.org/iepctsergio/api_archivo.git
WORKDIR /usr/src/app/api_archivo
RUN mkdir uploads
RUN npm install
CMD [ "npm", "start" ]