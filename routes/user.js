'use strict'

let express = require('express');
let api = express.Router();
let multipart = require('connect-multiparty');
let md_upload = multipart({ uploadDir: './uploads' });

let actas= require('../controllers/actas.controller');

//api.post('/archivos', md_upload, actas.agregarArchivo);
api.get('/consultar',actas.obtenerArchivo);

module.exports = api;