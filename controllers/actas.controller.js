'use strict'
let fs = require('fs');
let path = require('path');

function obtenerArchivo(req, res) {

    let imagen_file = req.query.archivo;
    let path_file = './uploads/' + imagen_file;
    if (path_file.length > 0) {
        fs.exists(path_file, function (exists) {
            if (exists) {
                res.sendFile(path.resolve(path_file));
            } else {
                res.status(200).send({ message: 'No existe la imagen' });
            }
        });
    } else {
        res.status(200).send({ message: 'No solicitó un archivo' });
    }

}

module.exports = {
    obtenerArchivo
}